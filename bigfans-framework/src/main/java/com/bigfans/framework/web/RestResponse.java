package com.bigfans.framework.web;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lichong 2015年8月25日上午9:06:56
 * @Description:用来包装rest请求返回的数据
 */
@Data
public class RestResponse {

    private Integer status;
    private String message;
    private String redirectUrl;
    private Object data;

    public static final Integer STATUS_UNAUTHORIZED = 401;

    @Data
    final class OKResponse{
        private Object data;
    }

    @Data
    final class ErrorResponse{
        private String errorCode;
        private String errorMessage;
    }


    public static RestResponse ok() {
        return ok(null);
    }

    public static RestResponse ok(Object data) {
        RestResponse resp = new RestResponse();
        resp.status = 200;
        if (data != null) {
            resp.data = data;
        }
        return resp;
    }

    public static RestResponse error(String message) {
        RestResponse resp = new RestResponse();
        resp.message = message;
        return resp;
    }

    public static RestResponse error(Integer status , String message) {
        RestResponse resp = new RestResponse();
        resp.status = status;
        resp.message = message;
        return resp;
    }

    public RestResponse() {
        this.status = ResponseStatus.SUCCESS;
    }
}
