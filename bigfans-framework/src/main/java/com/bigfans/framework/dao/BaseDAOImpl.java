package com.bigfans.framework.dao;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

import com.bigfans.framework.BeanProviderFactory;

public abstract class BaseDAOImpl {

	public final static int NO_ROW_OFFSET = 0;
	public final static int NO_ROW_LIMIT = Integer.MAX_VALUE;

	protected String className = this.getClass().getName();
	
	private SqlSessionTemplate sqlSession;
	private JdbcTemplate jdbcTemplate;
	private TransactionTemplate tx;

	protected SqlSession getSqlSession() {
		if (sqlSession == null) {
			sqlSession = BeanProviderFactory.getProvider().getBean(SqlSessionTemplate.class);
		}
		return sqlSession;
	}

	protected JdbcTemplate getJdbcTemplate() {
		if(jdbcTemplate == null){
			jdbcTemplate = BeanProviderFactory.getProvider().getBean(JdbcTemplate.class);
		}
		return jdbcTemplate;
	}
	
	protected TransactionTemplate getTx() {
		if(tx == null){
			tx = BeanProviderFactory.getProvider().getBean(TransactionTemplate.class);
		}
		return tx;
	}
}
