package com.bigfans.framework.cache;

import java.util.List;

import com.bigfans.framework.BeanProviderFactory;
import com.bigfans.framework.redis.JedisConnection;
import com.bigfans.framework.redis.JedisExecuteCallback;
import com.bigfans.framework.redis.JedisExecuteWithoutReturnCallback;
import com.bigfans.framework.redis.JedisTemplate;
import com.bigfans.framework.utils.IOUtils;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Feb 25, 2016 5:11:21 AM 
 *
 */
public class RedisCacheProvider implements CacheProvider {
	
	private Integer DB_INDEX_DBCACHE = 4;
	private JedisTemplate jedisTemplate ;

	public RedisCacheProvider(JedisTemplate jedisTemplate) {
		this.jedisTemplate = jedisTemplate;
	}

	public RedisCacheProvider(JedisTemplate jedisTemplate , int dbindex) {
		this.jedisTemplate = jedisTemplate;
		this.DB_INDEX_DBCACHE = dbindex;
	}

	@Override
	public CacheEntry get(final CacheKey key) {
		return jedisTemplate.execute(new JedisExecuteCallback<CacheEntry>() {
			@Override
			public CacheEntry runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX_DBCACHE);
				byte[] bytes = conn.get(key.getKey().getBytes());
				if(bytes == null){
					return null;
				}
				CacheEntry entry = new CacheEntry(); 
				entry.setValue(IOUtils.unserialize(bytes));
				entry.setKey(key);
				return entry;
			}
		});
	}

	@Override
	public void put(final CacheEntry cacheEntry) {
		jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
			@Override
			public void runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX_DBCACHE);
				conn.set(cacheEntry.getKey().getKey().getBytes(),IOUtils.serialize(cacheEntry.getValue()));
			}
		});
	}

	@Override
	public void update(final CacheEntry cacheEntry) {
		jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
			@Override
			public void runInConnection(JedisConnection conn) {
				RedisCacheProvider.this.evict(cacheEntry.getKey());
				RedisCacheProvider.this.put(cacheEntry);
			}
		});
	}

	@Override
	public void evict(final CacheKey key) {
		jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
			@Override
			public void runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX_DBCACHE);
				conn.del(key.getKey());
			}
		});
	}

	@Override
	public void evict(final List<CacheKey> keys) {
		jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
			@Override
			public void runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX_DBCACHE);
				for (CacheKey key : keys) {
					conn.del(key.getKey());
				}
			}
		});
	}

	@Override
	public void clear() {
		
	}
}
