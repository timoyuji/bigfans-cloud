package com.bigfans.userservice.listener;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.order.OrderCanceledEvent;
import com.bigfans.model.event.order.OrderCreateFailureEvent;
import com.bigfans.userservice.service.UserPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@KafkaConsumerBean
public class OrderListener {

    private Logger logger = LoggerFactory.getLogger(OrderListener.class);

    @Autowired
    private UserPropertyService userPropertyService;

    @KafkaListener
    public void on(OrderCreateFailureEvent event){
        try{
            String orderId = event.getOrderId();
            userPropertyService.revertOrderProperty(orderId);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }

    @KafkaListener
    public void on(OrderCanceledEvent event){
        try{
            String orderId = event.getOrderId();
            userPropertyService.revertOrderProperty(orderId);
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }
}
