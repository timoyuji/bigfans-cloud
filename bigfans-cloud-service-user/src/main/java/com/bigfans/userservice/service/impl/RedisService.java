package com.bigfans.userservice.service.impl;

import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.redis.JedisConnection;
import com.bigfans.framework.redis.JedisExecuteCallback;
import com.bigfans.framework.redis.JedisExecuteWithoutReturnCallback;
import com.bigfans.framework.redis.JedisTemplate;
import com.bigfans.framework.utils.DateUtils;
import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.framework.utils.StringHelper;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;


/**
 * 
 * @Title: 
 * @Description: Redis的业务逻辑
 * @author lichong 
 * @date 2016年2月1日 上午9:24:01 
 * @version V1.0
 */
public class RedisService {
	
	private Integer DB_INDEX = 1;
	
	private String PREFIX_SU = "login:";
	private String PREFIX_SESSION = "session:";
	
	private static final int DAYS_30 = 60 * 60 * 24 * 30;
	
	public JedisTemplate jedisTemplate;
	
	public RedisService(JedisTemplate jedisTemplate){
		this.jedisTemplate = jedisTemplate;
	}

	public void setDB_INDEX(Integer DB_INDEX) {
		this.DB_INDEX = DB_INDEX;
	}

	public void setAttribute(final String userKey , final String attrName , final Object value){
		Assert.notNull(userKey);
		Assert.notNull(attrName);
		Assert.notNull(value);
		jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
			public void runInConnection(JedisConnection conn) {
				String key = PREFIX_SESSION + userKey;
				conn.selectDB(DB_INDEX);
				Map<String , String> data = new HashMap<String , String>();
				data.put(attrName, JsonUtils.toJsonString(value));
				conn.hmset(key, data);
			}
		});
	}
	
	public String getAttribute(final String userKey , final String attrName){
		Assert.notNull(userKey);
		Assert.notNull(attrName);
		return jedisTemplate.execute(new JedisExecuteCallback<String>() {
			public String runInConnection(JedisConnection conn) {
				String key = PREFIX_SESSION + userKey;
				conn.selectDB(DB_INDEX);
				return conn.hget(key, attrName);
			}
		});
	}
	
	public void removeAllAttributes(final String userKey){
		Assert.notNull(userKey);
		jedisTemplate.executeInPipeline(new JedisExecuteWithoutReturnCallback() {
			public void runInConnection(JedisConnection conn) {
				String key = PREFIX_SESSION + userKey;
				conn.selectDB(DB_INDEX);
				conn.del(key);
			}
		});
	}
	
	public void removeAttribute(final String userKey , final String attrName){
		Assert.notNull(userKey);
		Assert.notNull(attrName);
		jedisTemplate.executeInPipeline(new JedisExecuteWithoutReturnCallback() {
			public void runInConnection(JedisConnection conn) {
				String key = PREFIX_SESSION + userKey;
				conn.selectDB(DB_INDEX);
				conn.hdel(key, attrName);
			}
		});
	}

	public void setSessionUser(final String userKey ,final CurrentUser su){
		Assert.notNull(userKey);
		Assert.notNull(su);
		jedisTemplate.executeInPipeline(new JedisExecuteWithoutReturnCallback() {
			public void runInConnection(JedisConnection conn) {
				String key = PREFIX_SU + userKey;
				conn.selectDB(DB_INDEX);
				Map<String , String> data = new HashMap<String , String>();
				data.put("id", su.getUid());
				data.put("nickname", su.getNickname());
				data.put("ip", su.getIp());
				data.put("lastLoginTime", DateUtils.toStringYMDHMS(su.getLoginTime()));
				conn.hmset(key, data);
			}
		});
	}
	
	public CurrentUser getSessionUser(final String userKey){
		if (StringHelper.isEmpty(userKey)){
			return null;
		}
		return jedisTemplate.execute(new JedisExecuteCallback<CurrentUser>() {
			public CurrentUser runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX);
				String key = PREFIX_SU + userKey;
				Boolean exists = conn.exists(key);
				if(!exists){
					return null;
				}
				CurrentUser su = new CurrentUser();
				su.setUid(conn.hget(key, "id"));
				su.setNickname(conn.hget(key, "nickname"));
				su.setIp(conn.hget(key, "ip"));
				su.setLoginTime(DateUtils.toDateYMDHMS(conn.hget(key, "lastLoginTime")));
				return su;
			}
		});
	}
	
	public void removeSessionUser(final String userKey){
		Assert.notNull(userKey);
		jedisTemplate.executeInPipeline(new JedisExecuteWithoutReturnCallback() {
			public void runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX);
				conn.del(PREFIX_SU + userKey);
			}
		});
	}

	public String getVerificationCode(String mobile){
		return jedisTemplate.execute(new JedisExecuteCallback<String>() {
			@Override
			public String runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX);
				return conn.get("verification_code_" + mobile);
			}
		});
	}

	public void setVerificationCode(String mobile , String code){
		jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
			@Override
			public void runInConnection(JedisConnection conn) {
				conn.selectDB(DB_INDEX);
				conn.set("verification_code_" + mobile , code);
			}
		});
	}
}
