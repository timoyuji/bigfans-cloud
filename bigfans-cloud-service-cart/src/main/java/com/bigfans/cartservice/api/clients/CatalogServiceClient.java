package com.bigfans.cartservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.cartservice.model.Product;
import com.bigfans.cartservice.model.ProductSpec;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Product> getProductById(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/attributes?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            Product product = BeanUtils.mapToModel((Map) response.getData() , Product.class);
            return product;
        });
    }

    public CompletableFuture<List<ProductSpec>> getSpecsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/specs?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            List list = (List) response.getData();
            List<ProductSpec> specs = new ArrayList<>();
            for(int i = 0;i<list.size() ; i++){
                ProductSpec productSpec = BeanUtils.mapToModel((Map) list.get(i) , ProductSpec.class);
                specs.add(productSpec);
            }
            return specs;
        });
    }
}
