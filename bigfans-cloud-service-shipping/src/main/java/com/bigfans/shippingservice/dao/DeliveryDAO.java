package com.bigfans.shippingservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.shippingservice.model.Delivery;

/**
 * 
 * @Description:配送DAO操作
 * @author lichong
 * 2015年1月17日下午7:36:35
 *
 */
public interface DeliveryDAO extends BaseDAO<Delivery> {

}
