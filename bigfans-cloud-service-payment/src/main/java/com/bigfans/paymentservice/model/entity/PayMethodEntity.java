package com.bigfans.paymentservice.model.entity;

import com.bigfans.framework.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:支付类型
 * @author lichong 
 * 2015年4月4日下午9:22:44
 *
 */
@Table(name="PayMethod")
public class PayMethodEntity extends AbstractModel {

	private static final long serialVersionUID = 8383310302524445443L;
	
	public static final String TYPE_ONLINE = "online";
	public static final String TYPE_OFFLINE = "offline";

	@Column(name="name")
	protected String name;
	@Column(name="parent_id")
	protected String parentId;
	@Column(name="type")
	protected String type;
	@Column(name="code")
	protected String code;
	@Column(name="url")
	protected String url;
	@Column(name="is_primary")
	protected Boolean isPrimary;
	@Column(name="order_num")
	protected Integer orderNum;
	@Column(name="icon")
	protected String  icon;

	public String getModule() {
		return "PayMethod";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
}
