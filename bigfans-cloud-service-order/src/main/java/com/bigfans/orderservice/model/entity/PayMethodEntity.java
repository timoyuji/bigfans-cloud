package com.bigfans.orderservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:支付类型
 * @author lichong 
 * 2015年4月4日下午9:22:44
 *
 */
@Data
@Table(name="PayMethod")
public class PayMethodEntity extends AbstractModel {

	private static final long serialVersionUID = 8383310302524445443L;
	
	public static final String TYPE_ONLINE = "online";
	public static final String TYPE_OFFLINE = "offline";

	@Column(name="name")
	protected String name;
	@Column(name="parent_id")
	protected String parentId;
	@Column(name="type")
	protected String type;
	@Column(name="code")
	protected String code;
	@Column(name="url")
	protected String url;
	@Column(name="is_primary")
	protected Boolean isPrimary;
	@Column(name="order_num")
	protected Integer orderNum;
	@Column(name="icon")
	protected String  icon;

	public String getModule() {
		return "PayMethod";
	}
	
}
