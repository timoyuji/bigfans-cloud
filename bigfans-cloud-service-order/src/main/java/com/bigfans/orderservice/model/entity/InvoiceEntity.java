package com.bigfans.orderservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 发票信息
 * 
 * @author lichong
 *
 */
@Data
@Table(name="Invoice")
public class InvoiceEntity extends AbstractModel {

	private static final long serialVersionUID = 6043345179943339067L;

	@Column(name="user_id")
	protected String userId;
	@Column(name="order_id")
	protected String orderId;
	@Column(name="type")
	protected String type;
	@Column(name="title")
	protected String title;
	@Column(name="title_type")
	protected String titleType;
	@Column(name="content_type")
	protected String contentType;
	@Column(name="phone")
	protected String phone;
	@Column(name="email")
	protected String email;

	@Override
	public String getModule() {
		return "Invoice";
	}

}
