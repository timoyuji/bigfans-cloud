package com.bigfans.orderservice.api;

import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.model.Order;
import com.bigfans.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-04-10 下午8:48
 **/
@RestController
public class OrderManageApi {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/orders/{orderId}")
    public RestResponse getById(@PathVariable(value = "orderId") String orderId) throws Exception {
        Order order = orderService.load(orderId);
        return RestResponse.ok(order);
    }

}
