package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Table;

/**
 * @author lichong
 * @create 2018-03-20 下午7:53
 **/
@Data
@Table(name = "Stock_Log")
public class StockEntity extends AbstractModel {

    private String prodId;
    private String pgId;
    private Integer rest;
    private Integer locked;

    @Override
    public String getModule() {
        return "Stock";
    }
}
