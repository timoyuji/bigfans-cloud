package com.bigfans.catalogservice.service.home;

import com.bigfans.catalogservice.dao.FloorDAO;
import com.bigfans.catalogservice.model.Category;
import com.bigfans.catalogservice.model.Floor;
import com.bigfans.catalogservice.model.Product;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.catalogservice.service.category.CategoryServiceImpl;
import com.bigfans.catalogservice.service.product.ProductService;
import com.bigfans.catalogservice.service.product.ProductServiceImpl;
import com.bigfans.framework.dao.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年8月8日下午8:21:04
 *
 */
@Service(FloorServiceImpl.BEAN_NAME)
public class FloorServiceImpl extends BaseServiceImpl<Floor> implements FloorService {

	public static final String BEAN_NAME = "homeService";
	
	@Resource(name = CategoryServiceImpl.BEAN_NAME)
	private CategoryService categoryService;
	@Resource(name = ProductServiceImpl.BEAN_NAME)
	private ProductService productService;
	
	private FloorDAO floorDAO;
	
	@Autowired
	public FloorServiceImpl(FloorDAO floorDAO) {
		super(floorDAO);
		this.floorDAO = floorDAO;
	}
	
	@Override
	public int createFloor(Floor floor) throws Exception {
		return floorDAO.insertFloor(floor);
	}

	@Override
	public List<Floor> listFloor() throws Exception {
		List<Floor> floorList = floorDAO.listFloor();
		if (floorList != null) {
			for (Floor floor : floorList) {
				String catId = floor.getCategoryId();
				// 设置需要显示的商品类别
				List<Category> catList = categoryService.listSubCats(catId);
				floor.setCatgeoryList(catList);
				if(catList != null && !catList.isEmpty()){
					String[] catIds = new String[catList.size()];
					for (int i = 0; i < catList.size(); i++) {
						catIds[i] = catList.get(i).getId();
					}
					// 具体需要显示的商品
					List<Product> productPage = productService.listByCategory(catIds, 0L ,10L);
					floor.setProductList(productPage);
				}
			}
		}
		return floorList;
	}

}
