package com.bigfans.catalogservice.service.tag;

import com.bigfans.catalogservice.model.Tag;
import com.bigfans.framework.dao.BaseService;

import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年2月29日 上午10:05:22 
 * @version V1.0
 */
public interface TagService extends BaseService<Tag>{

	List<Tag> listByProdId(String pid) throws Exception;
	
	List<Tag> listDuplicate(String value) throws Exception;
	
	/**
	 * 
	 * @param duplicates
	 * @return 最终的id
	 * @throws Exception
	 */
	String mergeDuplicates(List<Tag> duplicates) throws Exception;
	
	Integer increaseRelatedCount(String tagId , Integer increase) throws Exception;
	
}
