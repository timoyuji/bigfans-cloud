package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.ProductGroupDAO;
import com.bigfans.catalogservice.model.ProductGroup;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.framework.model.PageBean;
import org.springframework.stereotype.Repository;

@Repository(ProductGroupDAOImpl.BEAN_NAME)
public class ProductGroupDAOImpl extends MybatisDAOImpl<ProductGroup> implements ProductGroupDAO {

	public static final String BEAN_NAME = "productGroupDAO";

	@Override
	public PageBean<ProductGroup> pageByCategoryId(String[] categoryId, Long start, Long pagesize){
		return null;
	}

	@Override
	public String getDescription(String pgId , String prodId) {
		ParameterMap params = new ParameterMap();
		params.put("pgId", pgId);
		params.put("prodId", prodId);
		return getSqlSession().selectOne(className + ".getDescription", params);
	}
}
