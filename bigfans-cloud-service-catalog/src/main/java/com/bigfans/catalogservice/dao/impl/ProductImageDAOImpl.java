package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.ProductImageDAO;
import com.bigfans.catalogservice.model.ImageGroup;
import com.bigfans.catalogservice.model.ProductImage;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年1月5日上午9:03:11
 *
 */
@Repository(ProductImageDAOImpl.BEAN_NAME)
public class ProductImageDAOImpl extends MybatisDAOImpl<ProductImage> implements ProductImageDAO {

	public static final String BEAN_NAME = "productImageDAO";

	@Override
	public List<ImageGroup> listWithGroup(String pid) {
		ParameterMap params = new ParameterMap();
		params.put("pid", pid);
		return getSqlSession().selectList(className + ".listWithGroup", params);
	}
	
	@Override
	public ProductImage getThumb(String prodId) {
		ParameterMap params = new ParameterMap();
		params.put("prodId", prodId);
		params.put("orderNum", 1);
		params.put("type", "M");
		return getSqlSession().selectOne(className + ".load", params);
	}
}
