package com.bigfans.catalogservice.dao.impl;

import com.bigfans.catalogservice.dao.SkuDAO;
import com.bigfans.catalogservice.model.SKU;
import com.bigfans.framework.cache.Cacheable;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SKU
 * 
 * @Description:
 * @author lichong
 * @date Mar 7, 2016 4:04:31 PM
 *
 */
@Repository(SkuDAOImpl.BEAN_NAME)
public class SkuDAOImpl extends MybatisDAOImpl<SKU> implements SkuDAO {

	public static final String BEAN_NAME = "skuDAO";

	@Override
	@Cacheable
	public List<SKU> listByPgId(String pgId) {
		ParameterMap params = new ParameterMap();
		params.put("pgId", pgId);
		return getSqlSession().selectList(className +".list", params);
	}

	@Override
	@Cacheable
	public SKU getByValKey(String valKey) {
		ParameterMap params = new ParameterMap();
		params.put("valKey", valKey);
		return getSqlSession().selectOne(className +".load", params);
	}

	@Override
	@Cacheable
	public SKU getByPid(String pid) {
		ParameterMap params = new ParameterMap();
		params.put("pid", pid);
		return getSqlSession().selectOne(className +".load", params);
	}
}
