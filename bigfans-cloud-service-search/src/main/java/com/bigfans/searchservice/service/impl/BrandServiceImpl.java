package com.bigfans.searchservice.service.impl;

import com.bigfans.framework.es.ElasticTemplate;
import com.bigfans.framework.es.request.CreateIndexCriteria;
import com.bigfans.framework.es.request.CreateMappingCriteria;
import com.bigfans.framework.es.request.SearchCriteria;
import com.bigfans.framework.es.request.SearchResult;
import com.bigfans.framework.es.schema.DefaultIndexSettingsBuilder;
import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.searchservice.document.convertor.BrandDocumentConverter;
import com.bigfans.searchservice.model.Brand;
import com.bigfans.searchservice.schema.mapping.BrandMapping;
import com.bigfans.searchservice.service.BrandService;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.PrefixQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 品牌服务类
 * @author lichong 
 * @date 2015年12月13日 下午6:07:41 
 * @version V1.0
 */
@Service(BrandServiceImpl.BEAN_NAME)
public class BrandServiceImpl implements BrandService {

	public static final String BEAN_NAME = "brandService";

	@Autowired
	private ElasticTemplate elasticTemplate;

	public Brand getById(String brandId) {
		Brand b = elasticTemplate.getById(BrandMapping.INDEX, BrandMapping.TYPE, brandId, new BrandDocumentConverter());
		return b;
	}

	public void createIndexAndMapping() throws Exception {
		this.createIndex();
		this.createMapping();
	}

	public void createIndex() throws Exception {
		elasticTemplate.deleteIndex(BrandMapping.INDEX);
		if (!elasticTemplate.isIndexExists(BrandMapping.INDEX)) {
			CreateIndexCriteria criteria = new CreateIndexCriteria();
			criteria.setIndexName(BrandMapping.INDEX);
			criteria.setAlias(BrandMapping.ALIAS);
			criteria.setVersion(1L);
			criteria.setSettingsBuilder(new DefaultIndexSettingsBuilder());
			elasticTemplate.createIndex(criteria);
		}
	}

	public void createMapping() throws Exception {
		CreateMappingCriteria action = new CreateMappingCriteria();
		action.setMappingBuilder(new BrandMapping());
		action.setIndexName(BrandMapping.INDEX);
		action.setType(BrandMapping.TYPE);
		elasticTemplate.createMapping(action);
	}

	@Override
	public FTSPageBean<Brand> search(String keyword , String categoryId , int start , int pagesize) throws Exception {
		PrefixQueryBuilder nameQuery = QueryBuilders.prefixQuery(BrandMapping.FIELD_NAME, keyword);
		SearchCriteria searchCriteria = new SearchCriteria(nameQuery);
		if(StringHelper.isNotEmpty(categoryId)){
			BoolQueryBuilder catFilter = QueryBuilders.boolQuery();
			catFilter.should(QueryBuilders.termQuery(BrandMapping.FIELD_CATEGORIES, categoryId));
			catFilter.should(QueryBuilders.termQuery(BrandMapping.FIELD_CATEGORY_ID, categoryId));
			searchCriteria.setQueryFilter(catFilter);
		}
		searchCriteria.setIndex(BrandMapping.INDEX);
		searchCriteria.setType(BrandMapping.TYPE);
		searchCriteria.setFrom(start);
		searchCriteria.setSize(pagesize);

		SearchResult<Brand> result = elasticTemplate.search(searchCriteria, new BrandDocumentConverter());
		List<Brand> data = result.getData();
		FTSPageBean<Brand> pageBean = new FTSPageBean<>(data, result.getTotHitis());
		return pageBean;
	}
}
