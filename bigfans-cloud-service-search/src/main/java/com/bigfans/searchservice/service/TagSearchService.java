package com.bigfans.searchservice.service;

import com.bigfans.searchservice.model.Tag;

import java.util.List;

public interface TagSearchService {

	List<Tag> searchTagByKeyword(String keyword, int size) throws Exception;

}
