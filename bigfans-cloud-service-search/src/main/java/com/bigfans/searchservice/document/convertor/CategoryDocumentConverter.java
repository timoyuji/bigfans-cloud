package com.bigfans.searchservice.document.convertor;

import com.bigfans.framework.es.DocumentConverter;
import com.bigfans.framework.es.IndexDocument;
import com.bigfans.searchservice.model.Category;
import com.bigfans.searchservice.schema.mapping.CategoryMapping;

import java.util.Map;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月8日上午9:40:58
 *
 */
public class CategoryDocumentConverter implements DocumentConverter<Category> {

	@Override
	public Category toObject(Map<String, Object> dataMap) {
		Category category = new Category();
		category.setId((String)dataMap.get(CategoryMapping.FIELD_ID));
		category.setName((String)dataMap.get(CategoryMapping.FIELD_NAME));
		category.setDescription((String)dataMap.get(CategoryMapping.FIELD_PARENT_ID));
		category.setLevel((Integer)dataMap.get(CategoryMapping.FIELD_LEVEL));
		return category;
	}

	@Override
	public IndexDocument toDocument(Category obj) {
		return null;
	}

}
