package com.bigfans.searchservice.model;

import com.bigfans.framework.annotations.FtsEnable;
import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;


/**
 * 
 * @Title: 
 * @Description: 品牌
 * @author lichong 
 * @date 2015年12月13日 下午6:04:46 
 * @version V1.0
 */
@Data
public class Brand extends AbstractModel {

	private static final long serialVersionUID = 6251503210742416953L;
	
	protected String name;
	protected String logo;
	protected String categoryId;
	protected Boolean recommended;
	protected String description;
	protected Integer orderNum;
	
	public String getModule() {
		return "Brand";
	}
	
}
