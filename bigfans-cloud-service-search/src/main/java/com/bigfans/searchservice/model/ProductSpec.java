package com.bigfans.searchservice.model;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-25 下午2:00
 **/
@Data
public class ProductSpec {

    private String prodId;
    private String optionId;
    private String option;
    private String valueId;
    private String value;
    private String inputType;

}
